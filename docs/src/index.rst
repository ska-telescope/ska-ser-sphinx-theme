SKA Sphinx Theme
================

This project provides a Sphinx theme for the `SKA`_ documentation hosted on `Read the Docs`_.

It is based on the `Read the Docs Sphinx theme`_, but with:

* the SKA stylesheet applied;
* the SKA logo and favicon pre-set as defaults;
* The standard footer added.

Usage
-----
To apply this theme,

1. install ``ska-ser-sphinx-theme`` into the documentation build environment;
   that is, add it to your documentation dependencies.
2. In your ``conf.py``, set the ``html_theme`` property:

   .. code-block:: python

      html_theme = "ska_ser_sphinx_theme"

   Note that the package is named ``ska-ser-sphinx-theme`` (with hyphens),
   but the theme is named ``ska_ser_sphinx_theme`` (with underscores).

Upgrading
---------
If your project previously used ``sphinx_rtd_theme``
and directly customised it to apply the SKA style, logos, etc.,
the you have some cleaning up to do:

* Your ``conf.py`` contains some code to register a stylesheet. It will
  look something like:

   .. code-block:: python

      def setup(app):
          app.add_css_file("css/custom.css")

  or possibly just

   .. code-block:: python

      html_style = "css/custom.css"

  That can now be deleted, as can the file that it points to.

* Your ``conf.py`` specifies the logo, maybe through the ``html_logo`` or
  ``html_logo_url`` variable, or maybe through an entry in ``html_context``.
  This specification can now be deleted,
  as can the logo file that its points to.

* Your ``conf.py`` specifies your favicon in the same way.
  Delete it, and the favicon file that it points to.

* You may have a ``_templates`` folder containing template files
  such as ``layout.html`` or ``footer.html``. Delete them.

.. _SKA: https://skao.int/
.. _Read the Docs: https://readthedocs.org/
.. _Read the Docs Sphinx theme: https://sphinx-rtd-theme.readthedocs.io/en/stable/
