# ska-ser-sphinx-theme

A Sphinx RTD theme for SKA. Based on [sphinx-rtd-theme](https://sphinx-rtd-theme.readthedocs.io/en/stable/) but with extra SKA cowbell.

Documentation
-------------

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ser-sphinx-theme/badge/?version=latest)](https://developer.skao.int/projects/ska-ser-sphinx-theme/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-ser-sphinx-theme documentation](https://developer.skatelescope.org/projects/ska-ser-sphinx-theme/en/latest/index.html "SKA Developer Portal: ska-ser-sphinx-theme documentation")
