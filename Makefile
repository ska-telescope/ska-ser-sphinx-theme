include .make/base.mk

#############################
# Python
#############################
include .make/python.mk

PYTHON_LINE_LENGTH = 88
PYTHON_LINT_TARGET = src/

python-post-lint:
	mypy src/

.PHONY: python-post-lint


#############################
# Docs
#############################
include .make/docs.mk

DOCS_SPHINXOPTS=-W --keep-going

docs-pre-build:
	poetry install --only=main

.PHONY: docs-pre-build
