"""
A Sphinx ReadTheDocs theme for SKA.

Based on `sphinx-rtd-theme`, but with SKA cowbell.
"""

from pathlib import Path
from typing import Any, Dict

from sphinx.application import Sphinx

__version__ = "0.2.1"


def setup(app: Sphinx) -> Dict[str, Any]:
    """
    Register the theme and its extensions wih Sphinx.

    :param app: the Sphinx application.

    :return: a dictionary of information about the theme.
    """
    here = Path(__file__).parent.resolve()

    app.add_html_theme(name="ska_ser_sphinx_theme", theme_path=str(here))

    return {
        "version": __version__,
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
